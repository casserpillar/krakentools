# structure

# have folder of 
# 	name*#.png

# read directory
# send all to kraken
# find sequences
# extract parameters from filenames
# construct apng from parameters + pngs
# 	take less pngs if full apng > 500kb
# save apng with setname_same.apng
# create plist

# https://docs.python.org/3/library/os.html#os.scandir
# https://docs.python.org/3/library/os.html#os.DirEntry

# for entry in os.scandir(path):
#    if not entry.name.startswith('.') and entry.is_file():
#        print(entry.name)

# {
#     "success": true,
#     "file_name": "header.jpg",
#     "original_size": 324520,
#     "kraked_size": 165358,
#     "saved_bytes": 159162,
#     "kraked_url": "http://dl.kraken.io/d1/aa/cd/2a2280c2ffc7b4906a09f78f46/header.jpg"
# }

# https://api.kraken.io/v1/upload

# {
#     "auth": {
#         "api_key": "your-api-key",
#         "api_secret": "your-api-secret"
#     },
#     "wait": true
#     "lossy": true
#     "quality": 75

# }



import os
import threading import Thread
from requests_futures.sessions import FuturesSession
import urllib2


#checks if filepath is a png
def isPng(fileName):
	fileNameSplit = fileName.rsplit(sep=".", maxSplit=1)
	fileExtension = fileNameSplit[-1]
	if (fileExtension == "png"):
		return True
	return False

#starts an async kraken request
def getKraken(requestList, session, fileName, params):
	url = "https://api.kraken.io/v1/upload"
	requestParams = params
	files = {"file": open("fileName", 'rb')}
	requestList.append(session.post(url, params=params, files=files))

#saves an image from a url
def getImage(response):
	url = response["kraked_url"]
	responseName = response["file_name"]
	imageFile = urllib2.urlopen(url)
	resultName = "kraken_".append(responseName)
	with open(resultName, "wb") as outputFile:
		outputFile.write(imageFile.read())
	return resultName


def main():
	
	#Set the directory
	if(len(sys.argv) == 1):
		if(os.path.exists(sys.argv[0])):
			runPath = sys.argv[1]
		else:
			print("bad folder path")
			return
	else:
		runPath = os.getcwd()

	if (runPath[-1] != "/"):
		runPath.append("/")
		
	#Get all png files in the directory
	fileList = []
	for entry in os.scandir(runPath):
		if not entry.name.startswith('.') and entry.is_file() and isPng(entry.name):
			fileList.append(runPath.append(entry.name))

	#spawn an async request to each png file
	requestList = []
	params = {"auth" : {"api_key": "your-key", "api_secret": "your-secret"}, "wait": true, "lossy": true, "quality": 90 }
	session = FuturesSession()
	for fileName in fileList:
		getKraken(requestList, session, fileName, params)

	#check all open requests and wait for them to respond, save the resulting images
	resultNames = []
	for openRequest in requestList:
		response = openRequest.result() #handling
		if (response["success"]):
			resultName = getImage(response)
			resultNames.append(resultName)

	#construct an apng
	##get sets of png files
	##for sets with size > 1, combine into apng
	##save apngs in a format
		






